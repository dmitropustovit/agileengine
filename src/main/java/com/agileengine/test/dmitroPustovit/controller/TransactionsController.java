package com.agileengine.test.dmitroPustovit.controller;

import com.agileengine.test.dmitroPustovit.model.AddTransactionRequest;
import com.agileengine.test.dmitroPustovit.model.NoCreditPossibleException;
import com.agileengine.test.dmitroPustovit.model.Transaction;
import com.agileengine.test.dmitroPustovit.repository.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

@RestController()
@RequestMapping(path = "/api/users",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionsController {

    private final TransactionsRepository transactionsRepository;

    @Autowired
    public TransactionsController(TransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    @RequestMapping(path = "")
    public double getUserBalance() {
        return transactionsRepository.getBalance();
    }

    @RequestMapping(path = "/transactions")
    public Collection<Transaction> getTransactionsHistory() {
        return transactionsRepository.get();
    }

    @RequestMapping(path = "/transactions/{uuid}")
    public ResponseEntity<Transaction> getUserTransaction(@PathVariable String uuid) {
        Optional<Transaction> transaction = transactionsRepository.get(uuid);
        return transaction.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @RequestMapping(path = "/transactions", method = RequestMethod.POST)
    public ResponseEntity getUserTransaction(@RequestBody AddTransactionRequest request) {
        try {
            return new ResponseEntity<>(transactionsRepository.add(request.getAmount(), request.getTransactionType()), HttpStatus.OK);
        } catch (NoCreditPossibleException ex) {
            return new ResponseEntity<>("Your balance is too low for this transaction", HttpStatus.METHOD_NOT_ALLOWED);
        }
    }
}
