package com.agileengine.test.dmitroPustovit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmitroPustovitApplication {

	public static void main(String[] args) {
		SpringApplication.run(DmitroPustovitApplication.class, args);
	}

}
