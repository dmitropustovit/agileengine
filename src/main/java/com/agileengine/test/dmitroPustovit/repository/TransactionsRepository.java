package com.agileengine.test.dmitroPustovit.repository;

import com.agileengine.test.dmitroPustovit.model.Transaction;
import com.agileengine.test.dmitroPustovit.model.TransactionType;

import java.util.Collection;
import java.util.Optional;

public interface TransactionsRepository {

    Transaction add(double amount, TransactionType transactionType);

    Collection<Transaction> get();

    Optional<Transaction> get(String uuid);

    double getBalance();
}
