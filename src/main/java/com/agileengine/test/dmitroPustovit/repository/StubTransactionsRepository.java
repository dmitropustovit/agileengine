package com.agileengine.test.dmitroPustovit.repository;

import com.agileengine.test.dmitroPustovit.model.NoCreditPossibleException;
import com.agileengine.test.dmitroPustovit.model.Transaction;
import com.agileengine.test.dmitroPustovit.model.TransactionType;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Repository
public class StubTransactionsRepository implements TransactionsRepository {

    private Map<String, Transaction> transactions = new HashMap<>();
    private double balance = 0;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();


    @Override
    public Transaction add(double amount, TransactionType transactionType) {
        writeLock.lock();
        try {
            double newBalance = balance + amount * transactionType.multiplier;

            if (newBalance < 0) {
                throw new NoCreditPossibleException();
            }
            balance = newBalance;
            Transaction transaction = new Transaction();
            transaction.setAmount(amount);
            transaction.setTransactionType(transactionType);
            transactions.put(transaction.getUuid(), transaction);
            return transaction;
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public Collection<Transaction> get() {
        readLock.lock();
        try {
            return transactions.values();
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public Optional<Transaction> get(String uuid) {
        readLock.lock();
        try {
            return Optional.ofNullable(transactions.get(uuid));
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public double getBalance() {
        readLock.lock();
        try {
            return balance;
        } finally {
            readLock.unlock();
        }
    }
}
