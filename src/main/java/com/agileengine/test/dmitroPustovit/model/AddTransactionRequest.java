package com.agileengine.test.dmitroPustovit.model;

public class AddTransactionRequest {

    private double amount;
    private TransactionType transactionType;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
