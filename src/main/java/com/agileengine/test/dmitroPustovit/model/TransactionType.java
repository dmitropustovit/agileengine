package com.agileengine.test.dmitroPustovit.model;

public enum TransactionType {
    CREDIT(-1),
    DEBIT(1);

    public final int multiplier;

    TransactionType(int multiplier) {
        this.multiplier = multiplier;
    }
}
