package com.agileengine.test.dmitroPustovit.model;

import java.util.Date;
import java.util.UUID;

public class Transaction {

    private String uuid;
    private double amount;
    private TransactionType transactionType;
    private long effectiveDate;

    public Transaction() {
        uuid = UUID.randomUUID().toString();
        effectiveDate = new Date().getTime();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public long getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(long effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
}
