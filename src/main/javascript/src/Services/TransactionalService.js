class TransactionalService {

    getTransactions() {
        return fetch("/api/users/transactions", {
            headers: {'Content-Type': 'application/json'}
        });
    }
}

export default new TransactionalService();