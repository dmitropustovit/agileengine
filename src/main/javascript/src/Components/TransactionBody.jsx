import React from 'react';
import Moment from "react-moment";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";

export default function TransactionBody(props) {
    const {transaction, index} = props;
    return (
        <Accordion.Collapse eventKey={index}>
            <Card.Body>
                <div>Type: {transaction.transactionType}</div>
                <div>Amount: {transaction.amount}</div>
                <div>Date:
                    <Moment format='YYYY/MM/DD HH:MM:SS'>
                        {new Date(transaction.effectiveDate)}
                    </Moment></div>
                <div>ID: {transaction.uuid}</div>
            </Card.Body>
        </Accordion.Collapse>
    );
}