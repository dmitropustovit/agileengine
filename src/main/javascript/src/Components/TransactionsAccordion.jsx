import React from "react";
import TransactionalService from "../Services/TransactionalService";
import Accordion from "react-bootstrap/Accordion";
import TransactionBody from "./TransactionBody";
import TransactionHeader from "./TransactionHeader/TransactionHeader";
import Card from "react-bootstrap/Card";

export default class TransactionsAccordion extends React.Component {

    state = {};

    componentDidMount() {
        TransactionalService.getTransactions()
            .then(response => response.json())
            .then(res => this.setState({transactions: res.sort((a, b) => a - b)}))
            .catch(res => this.setState({error: res}));
    }

    render() {
        const {transactions, error} = this.state;

        if (!transactions && !error) {
            return 'Loading....';
        }
        if (error) {
            return (
                <div className={'error'}> We get some error, please try again later or contact administrator</div>
            )
        }
        if (transactions.length === 0) {
            return 'You don\'t have any transactions. Please make some transaction to see your history';
        }
        return (
            <div>
                <Accordion>
                    {transactions.map((transaction, index) =>
                        <Card key={transaction.uuid}>
                            <TransactionHeader transaction={transaction} index={index}/>
                            <TransactionBody transaction={transaction} index={index}/>
                        </Card>
                    )}
                </Accordion>
            </div>
        )
    }
}