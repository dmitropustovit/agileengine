import React from 'react';
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import './TransactionHeader.css';

const CLASS_NAME = 'transaction-header';

export default function TransactionHeader(props) {
    const {transaction, index} = props;


    return (
        <Accordion.Toggle className={`${CLASS_NAME} ${CLASS_NAME}--${transaction.transactionType.toLocaleLowerCase()}`}
                          as={Card.Header}
                          eventKey={index}>
            {transaction.transactionType} : {transaction.amount}
        </Accordion.Toggle>
    );
}