import React from 'react';
import './App.css';
import TransactionsAccordion from "./Components/TransactionsAccordion";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <TransactionsAccordion/>
            </header>
        </div>
    );
}

export default App;
