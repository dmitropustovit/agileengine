Run Application : 
java -jar dmitroPustovitTest.jar

backend sources could be found in "src/main/java"

frontend sources could be found in "src/main/javascript"

Web application will be available by address - localhost:8080

All API calls use application/json Content-type.


API : 

* GET: /api/users - retrieve user balance  

* GET: /api/users/transactions - retrieve user transaction history 

* GET: /api/users/transactions/{transactionUuid} - retrieve transaction by uuid 

* POST: /api/users/transactions - make transaction. In case is current balance lower then CREDIT operation, user will receive 405 error  


Notes:

* For react application I didn't use any state managers (like Redux), because application is simple 

* For react application I didn't use TC typing 

* For backend - In theory there are should be additional level between Controller and Repository (smth like business layer), but again application is simple and this level dropped

* In current project NO auto build tool and final binary was built manually from two modules - FE + BE 

* Missed .gitignore file. I don't want configure it for one commit 